from keras import preprocessing
from keras.layers import Embedding, Flatten, Dense
from keras.datasets import imdb
from keras.models import Sequential

# Embedding_Layer = Embedding(1000, 64)

max_features = 10000
max_len = 20
(X_train, Y_train), (X_test, Y_test) = imdb.load_data(num_words=max_features)
X_train = preprocessing.sequence.pad_sequences(X_train, maxlen=max_len)
# X_test = preprocessing.sequence.pad_sequences(X_test, maxlen=max_len)

model = Sequential()

model.add(Embedding(10000, 8, input_length=max_len))
model.add(Flatten())
model.add(Dense(1, activation="sigmoid"))
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
history = model.fit(X_train, Y_train, epochs=10,
                    batch_size=32, validation_split=0.2)
