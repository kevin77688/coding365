from sklearn.datasets import make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib import pyplot

X, Y = make_classification(n_samples=1000, n_classes=2, weights=[1, 1],
                           random_state=1)
trainX, testX, trainY, testY = train_test_split(
    X, Y, test_size=0.5, random_state=2)
model = KNeighborsClassifier(n_neighbors=3)
model.fit(trainX, trainY)
probs = model.predict_proba(testX)
probs = probs[:, 1]

auc = roc_auc_score(testY, probs)
print('AUC: %.3f' % auc)
fpr, tpr, thresholds = roc_curve(testY, probs)
pyplot.plot([0, 1], linestyle='--')
pyplot.plot(fpr, tpr, marker='.')
pyplot.show()
