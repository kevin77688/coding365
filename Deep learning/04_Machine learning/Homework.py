import csv
from matplotlib import pyplot as plt
import numpy as np

Stressed_X = []
Stressed_Y = []
with open('Data/Stressed.csv') as S:
    readCSV = csv.reader(S, delimiter=',')
    row = iter(readCSV)
    next(row, None)
    for row in readCSV:
        Stressed_X.append(float(row[0]))
        Stressed_Y.append(float(row[1]))

N_Stressed_X = []
N_Stressed_Y = []
with open('Data/NotStressed.csv') as S:
    readCSV = csv.reader(S, delimiter=',')
    row = iter(readCSV)
    next(row, None)
    for row in readCSV:
        N_Stressed_X.append(float(row[0]))
        N_Stressed_Y.append(float(row[1]))

# minimum_X = min(min(Stressed_X), min(N_Stressed_X))
# Maximum_X = max(max(Stressed_X), max(N_Stressed_X))
# minimum_Y = min(min(Stressed_Y), min(N_Stressed_Y))
# Maximum_Y = max(max(Stressed_Y), max(N_Stressed_Y))

plt.scatter(Stressed_X, Stressed_Y, color='red')
plt.scatter(N_Stressed_X, N_Stressed_Y, color='blue')
plt.xticks(np.arange(0, 15, step=2))
plt.xlabel('wife')
plt.yticks(np.arange(0, 25, step=5))
plt.ylabel('husband')
plt.show()
