var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://127.0.0.1')

client.on('connect', function() {
  client.publish('hello', 'Hi')
  client.publish('hello', 'Hi again')
  client.publish('abc', 'whut?')
  client.publish('presence', 'presence test')
  console.log('Disconnected : ')
  client.end()
})