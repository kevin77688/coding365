var ws = new WebSocket("ws://localhost:8080");
ws.onopen = function(e) {
  console.log('Connection to server opened');
}

ws.onmessage = function(event) {
  console.log('Client recieved : ', event);
  document.getElementById('recieved').innerHTML = event.data
}

ws.onclose = function(e) {
  console.log('connection closed !');
}

function sendMessage() {
  ws.send(document.getElementById('text').value);
}
