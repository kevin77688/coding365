var WebSocketServer = require('ws').Server;
wss = new WebSocketServer({
  port: 8080
});

var clients = [];
wss.on('connection', function(ws) {
  clients.push(ws);

  ws.on('message', function(message) {
    console.log(message);
    ws.send('Server recieved : ' + message + '\t:)');
  });



  ws.on('close', function(message) {
    clients = clients.filter(function(ws) {
      return ws;
    });
  });
});

console.log('Running');
