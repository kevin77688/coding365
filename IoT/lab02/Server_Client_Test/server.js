// import net module
var net = require('net');

//Create server

var server = net.createServer(function(client) {


  console.log('Client connect. Client local address : ' +
    client.localAddress + ':' + client.localPort + '.client remote address : ' +
    client.remoteAddress + ':' + client.remotePort);

  client.on('data', function(data) {
    console.log('Recieve data : ' + data + 'data size : ' + client.bytesRead);
    client.end('Server recieved data : ' + data + ', send back to client data size : ' + client.bytesWritten);
  });

  client.on('end', function() {
    console.log('Client disconnected');
  });

});

server.listen(9999, function() {
  console.log('server listen on port 9999');
});