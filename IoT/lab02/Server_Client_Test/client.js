var net = require('net');


function getConnection(connName) {
  var option = {
    host: 'localhost',
    port: 9999
  }

  var client = net.createConnection(option, function() {
    console.log('Connection name : ' + connName);
    console.log('Connection local address : ' +
      client.localAddress + ':' + client.localPort);
    console.log('Connection remote address : ' +
      client.remoteAddress + ':' + client.remotePort);
  });

  client.on('data', function(data) {
    console.log('Server return : ' + data);
  });

  client.on('end', function() {
    console.log('Client socket disconnected');
  });

  return client;
}


try {
  // getConnection('Java')
  var Client = getConnection('Java');
  Client.write('Coding365 has a lot of fun.');
} catch (err) {
  console.log('Error : ' + err.message);
}